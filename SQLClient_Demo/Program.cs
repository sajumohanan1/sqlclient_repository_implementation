﻿using SQLClient_Demo.Models;
using SQLClient_Demo.Repository;
using System;
using System.Collections.Generic;

namespace SQLClient_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Work flow
             * 1. Setup the connection
             * 2. Repository (Interface) - loose coupling
             * 3. CRUD (Create, Read, Update, Delete)
             * 4. Implementing the non-queries
             * 5. Reactoring through the generic interfaces (Optional)
             */
            IProfessorRepository repository = new ProfessorRepository();

            PrintAllRecords(repository);
            //PrintOneRecord(repository);
            //InsertRecord(repository);
            //UpdateRecord(repository);
            //DeleteRecord(repository);

            Console.ReadKey();
        }

        private static void PrintAllRecords(IProfessorRepository repository)
        {
            PrintProfessors(repository.GetAllProfessors());
        }

        static void PrintOneRecord(IProfessorRepository repository)
        {
            //Input for the testing
            PrintProfessor(repository.GetProfessor("P5"));
        }

        static void InsertRecord(IProfessorRepository repository)
        {
            Professor test = new Professor()
            {
                ID = "P5",
                FirstName="saju",
                LastName="mohanan",
                Subject="Information Technology"
            };
            if (repository.AddNewProfessor(test))
            {
                Console.WriteLine("Successfully inserted record");
                PrintProfessor(repository.GetProfessor("P4"));
            }
            else
            {
                Console.WriteLine("not successful");
            }
        }

        static void UpdateRecord(IProfessorRepository repository)
        {
            Professor update = new Professor()
            {
                ID = "P4",
                FirstName = "first name",
                LastName = "Last name",
                Subject = "any subject"
            };
            if (repository.UpdateProfessor(update))
            {
                Console.WriteLine("Successfully updated record");
                PrintProfessor(repository.GetProfessor("P4"));
            }
            else
            {
                Console.WriteLine("not successful");
            }
        }

        static void DeleteRecord(IProfessorRepository repository)
        {
            repository.DeleteProfessor("ZZZZZ");
        }

        static void PrintProfessors(IEnumerable<Professor> professors)
            //for printing all professors
        {
            foreach (Professor professor in professors)
            {
                PrintProfessor(professor); //Invode this method and print cw statement
            }
        }
        static void PrintProfessor(Professor professor)
            // To print only 1 professor
        {
            Console.WriteLine($"{professor.ID}, {professor.FirstName}, {professor.LastName}, {professor.Subject}");
        }
    }
}
