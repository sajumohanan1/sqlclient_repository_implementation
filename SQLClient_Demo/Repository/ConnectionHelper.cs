﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClient_Demo.Repository
{
    public class ConnectionHelper
    {
        public static string GetConnectionString()
        {
            //ConnectionStringBuilder - created connection string buildiner.
            SqlConnectionStringBuilder connectStringBuilder = new SqlConnectionStringBuilder();
            connectStringBuilder.DataSource = "SAJU-MOHANAN-NO\\SQLEXPRESS";
            connectStringBuilder.InitialCatalog = "UniversityDB";
            connectStringBuilder.IntegratedSecurity = true;
            return connectStringBuilder.ConnectionString;
        }
    }
}
