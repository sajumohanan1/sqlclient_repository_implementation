﻿using SQLClient_Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClient_Demo.Repository
{
    public interface IProfessorRepository
    {      
        public List<Professor> GetAllProfessors(); //To get all the professors
        public Professor GetProfessor(string id); //To get a single professor

        public bool AddNewProfessor(Professor professor);
        public bool UpdateProfessor(Professor professor);
        public bool DeleteProfessor(string id);


    }
}
