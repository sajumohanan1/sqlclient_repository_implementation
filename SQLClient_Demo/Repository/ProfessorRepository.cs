﻿using Microsoft.Data.SqlClient;
using SQLClient_Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClient_Demo.Repository
{
    public class ProfessorRepository : IProfessorRepository
    {
        public bool AddNewProfessor(Professor professor)
        {
            bool success = false;
            string sql = "Insert into Professor(ID, FirstName, LastName, Subject) Values(@ID, @FirstName, @LastName, @Subject)";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID",professor.ID);
                        cmd.Parameters.AddWithValue("@FirstName",professor.FirstName);
                        cmd.Parameters.AddWithValue("@LastName",professor.LastName);
                        cmd.Parameters.AddWithValue("@Subject",professor.Subject);
                        //Whenever there is a change in Database - ExecuteNonQuery()
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return success;
        }

        public bool DeleteProfessor(string id)
        {
            bool success = false;
            string sql = "Delete from Professor where ID=@ID";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", id);                       
                        //Whenever there is a change in Database - ExecuteNonQuery()
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return success;
        }

        public List<Professor> GetAllProfessors()
        {
            //To get all professors from the database
            List<Professor> profList = new List<Professor>();
            //Select statement
            string sql = "select ID, FirstName, LastName, Subject From Professor";
            try
            {
                //1. Try to connect with the database - SqlConnection
                //2. SqlCommand - to create a command
                //3. SqlDataReader (data set) - 
                //4. Handle the request
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Professor temp = new Professor();
                                //Assign value from database to the ID
                                temp.ID = reader.GetString(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Subject = reader.GetString(3);
                                //Add to the collection List.
                                profList.Add(temp);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return profList;
        }

        public Professor GetProfessor(string id)
        {
            Professor professor = new Professor();
                        
            string sql = "select ID, FirstName, LastName, Subject From Professor where ID=@ID";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID",id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                professor.ID = reader.GetString(0);
                                professor.FirstName = reader.GetString(1);
                                professor.LastName = reader.GetString(2);
                                professor.Subject = reader.GetString(3);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return professor;
        }

        public bool UpdateProfessor(Professor professor)
        {
            bool success = false;
            string sql = "update Professor set ID=@ID, FirstName=@FirstName," +
                "LastName=@LastName,Subject=@Subject where ID=@ID";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", professor.ID);
                        cmd.Parameters.AddWithValue("@FirstName", professor.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", professor.LastName);
                        cmd.Parameters.AddWithValue("@Subject", professor.Subject);
                        //Whenever there is a change in Database - ExecuteNonQuery()
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return success;
        }
    }
}
